Slyde allow setting values for options that were not intended by mojang (for example: ridiculous fov effects, gamma, render distance)

This will (obviously) break things if you are not careful (for example: changing the target resolution outside its slider will cause a crash)